# Prune

Prune is a program for finding and deleting timestamped files or directories
based on a given retention plan. It is useful for example for keeping a certain
set of backups on disk and deleting the rest.

This project contains both the library and the executable program.


## How it works

Prune understands daily, weekly, monthly and yearly item categories. When told
to keep `n` daily items, it keeps only the latest item for each day and marks
the rest to be pruned. It goes back in the chronological item order as long as
is needed for `n` daily items to be found. The same logic is applied for each
of the categories: weekly backups are the latest backup of each of the last
weeks etc.

The items are matched to categories in order from most recent to least recent.
That means that for each item it is first checked if it is a daily item that
should be kept. If not, then it is checked if the item is weekly item etc. If
the item is not placed in any of the categories, it is marked to be pruned.
The number of items that are kept is at most the sum of the items in the given
retention plan.

Prune currently looks for files or directories only in the format
YYYY-MM-DDTHH-MM-SS (i.e. strftime `"%Y-%m-%dT%H-%M-%S"`).


## Examples

In this example we have gathered ten daily items. We want to keep three daily
items and two weekly items (five in total), the rest should be pruned:

```sh
$ ls
2020-10-01T12-00-00 2020-10-02T12-00-00 2020-10-03T12-00-00
2020-10-04T12-00-00 2020-10-05T12-00-00 2020-10-06T12-00-00
2020-10-07T12-00-00 2020-10-08T12-00-00 2020-10-09T12-00-00
2020-10-10T12-00-00

$ prune --plan "0,0,2,3" # 0 yearly, 0 monthly, 2 weekly, 3 daily
./2020-10-01T12-00-00
./2020-10-02T12-00-00
./2020-10-03T12-00-00
./2020-10-05T12-00-00
./2020-10-06T12-00-00
```

By default, the files (or directories) to be pruned are printed and deleted. As
you can see, the three daily items are for the last three days, the one before
them (October 7th, Wednesday) is selected to be "this week's daily item" and
the item for October 4th (Sunday) is selected to be the previous week's weekly
item.

More verbose output can be selected by adding `--verbose` to the CLI options. If
you only want to preview what would be pruned without actually deleting
anything, use the `--dry-run` switch.


## Documentation

For the library (crate) documentation, build the documentation with `cargo doc`
or dive into the sources beginning in `src/lib.rs`.

For help with the binary program, build the executable and run `prune help`.


## Installation

Build the project, stript the debug symbols and install the resulting binary:

```
cargo build --release
strip target/release/prune
sudo install -m 0755 target/release/prune /usr/local/bin
```


## Bugs and Contribution

If you think you have found a bug, file an issue on the project page in GitLab:
<https://gitlab.com/harski/prune>.

Patches are welcome. If you have an idea on how to make the program better,
please open a issue first before implementing it. That way the idea can first
be discussed, improved or rejected before anyone has commited on doing the
work.


## License

This program is licensed under the two-clause BSD license. See the supplied
LICENSE file for details.
