# Changelog

All notable changes to this project since version 1.0 will be documented in
this file.

The format is based on [Keep a Changelog]
(https://keepachangelog.com/en/1.0.0/), and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## 1.0.4 - 2022-09-29

### Changed

- Update argument parsing library clap to version 4.
- Add `trycmd` test suite for CLI tests.


## 1.0.3 - 2022-09-28

### Changed

- Allow whitespace in cli plan strings (i.e. -p "5, 6, 4, 3").
- Fix compilation error caused by updated rust compiler (DirectoryNotEmpty).
- Update code to rust 2021 edition.
- Update chained dependencies.


## 1.0.2 - 2020-10-26

### Changed

- Performance and memory usage improvements.
- Documentation improvements.
- Enhance error reporting for file deletion errors.


## 1.0.1 - 2020-10-15

### Changed

- Updated Cargo.lock to rust 1.47 format.
