//! `Breakdown` contains the crate's root structure `Breakdown` and associated features.
//!
//! The `struct Plan` is used solely for passing `Item` category sizes to the `Breakdown` structure.
//!
//! The `struct Breakdown` in turn is the root data structure for the library. All `Item`s to be
//! handled should be passed into the `Breakdown` for categorizing, and the result `Vec`tors can be
//! read out from it.

use crate::error::Error;
use crate::item::Item;
use chrono::Datelike;
use std::str::FromStr;

/// `Plan` contains the data on how many `Item`s of each category should be retained.
///
/// The `Plan` is a simple container structure for holding category sizes. It does not have any
/// associated functionality itself.
///
/// The plan in the example below keeps at most `5 + 6 + 4 + 7 = 22` items.
///
/// ```
/// # use prune::Plan;
/// let plan = Plan {
///     yearly: 5,
///     monthly: 6,
///     weekly: 4,
///     daily: 7
/// };
/// ```
#[derive(Debug)]
pub struct Plan {
    pub yearly: usize,
    pub monthly: usize,
    pub weekly: usize,
    pub daily: usize,
}

/// Item set container, divided to different `Plan` categories.
///
/// The Breakdown is the "root" data structure for `prune`. The `Item`s are compared and sorted
/// into different categories depending on their time stamps. All the `Item`s that do not match the
/// selected `Plan` (i.e. "extra" `Item`s) are put in to the `prune` category.
#[derive(Default, Debug)]
pub struct Breakdown {
    pub yearly: Vec<Item>,
    pub monthly: Vec<Item>,
    pub weekly: Vec<Item>,
    pub daily: Vec<Item>,
    pub prune: Vec<Item>,
}

impl Breakdown {
    /// Add Item to Breakdown.
    ///
    /// First try to insert the Item to the daily category, and if it is not a daily item try if it
    /// fits to next (weekly) category etc.
    fn add(&mut self, item: Item, plan: &Plan) {
        let _: Option<Item> = add_generic(&mut self.daily, item, |a, b| a == b, plan.daily)
            .and_then(|i| {
                add_generic(
                    &mut self.weekly,
                    i,
                    |a, b| a.iso_week() == b.iso_week(),
                    plan.weekly,
                )
            })
            .and_then(|i| {
                add_generic(
                    &mut self.monthly,
                    i,
                    |a, b| a.month() == b.month(),
                    plan.monthly,
                )
            })
            .and_then(|i| {
                add_generic(
                    &mut self.yearly,
                    i,
                    |a, b| a.year() == b.year(),
                    plan.yearly,
                )
            })
            .and_then(|i| {
                self.prune.push(i);
                self.prune.sort();
                None
            });
    }

    /// Create new Breakdown from Items and a Plan.
    ///
    /// ```no_run
    /// use prune::{read_items, Breakdown, Plan};
    /// use std::path::PathBuf;
    ///
    /// let items = read_items(&PathBuf::from(".")).unwrap();
    /// let plan = Plan::default();
    /// let breakdown = Breakdown::new(items, &plan);
    /// ```
    pub fn new(items: Vec<Item>, plan: &Plan) -> Breakdown {
        let mut bd = Breakdown {
            yearly: Vec::with_capacity(plan.yearly + 1),
            monthly: Vec::with_capacity(plan.monthly + 1),
            weekly: Vec::with_capacity(plan.weekly + 1),
            daily: Vec::with_capacity(plan.daily + 1),
            prune: Vec::new(),
        };

        for item in items {
            bd.add(item, plan);
        }

        bd
    }
}

/// Add an Item to the Breakdown category `vec`.
fn add_generic<F>(vec: &mut Vec<Item>, item: Item, date_comparator: F, cnt: usize) -> Option<Item>
where
    F: Fn(&chrono::NaiveDate, &chrono::NaiveDate) -> bool,
{
    vec.push(item);
    vec.sort();
    match generic_prune(vec, date_comparator) {
        Some(i) => Some(i),
        None => {
            if vec.len() > cnt {
                Some(vec.remove(0))
            } else {
                None
            }
        }
    }
}

/// Prune (if necessary) an item from the Breakdow category `vec`.
fn generic_prune<F>(vec: &mut Vec<Item>, equals: F) -> Option<Item>
where
    F: Fn(&chrono::NaiveDate, &chrono::NaiveDate) -> bool,
{
    for (i, item1) in vec.iter().enumerate() {
        // bail out if at the end of vector
        if i + 1 == vec.len() {
            break;
        }

        // check constraint
        let item2 = &vec[i + 1];
        if equals(&item1.date(), &item2.date()) {
            return Some(vec.remove(i));
        }
    }

    None
}

/// Default, fairly conservative Plan.
impl Default for Plan {
    fn default() -> Plan {
        Plan {
            yearly: 10,
            monthly: 12,
            weekly: 8,
            daily: 7,
        }
    }
}

impl FromStr for Plan {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut plan = Plan::default();
        let parts = s.split(',').map(|part| part.trim()).collect::<Vec<&str>>();
        if parts.len() != 4 {
            return Err(Error::ParsePlan);
        }

        // get plan data
        plan.yearly = parts[0].parse()?;
        plan.monthly = parts[1].parse()?;
        plan.weekly = parts[2].parse()?;
        plan.daily = parts[3].parse()?;
        Ok(plan)
    }
}
