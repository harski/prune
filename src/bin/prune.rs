extern crate clap;
extern crate prune;

use clap::{builder::ArgAction, Arg};
use prune::{read_items, Breakdown, Item, Plan};
use std::path::{Path, PathBuf};

const BIN_NAME: &str = env!("CARGO_PKG_NAME");
const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    // parse CLI arguments
    let args = clap::Command::new(BIN_NAME)
        .version(VERSION)
        .arg(Arg::new("DIR").help("Directory to prune").index(1))
        .arg(
            Arg::new("dry-run")
                .short('n')
                .long("dry-run")
                .help("Pretend mode, don't actually delete anything.")
                .num_args(0)
                .action(ArgAction::SetTrue),
        )
        .arg(
            Arg::new("plan")
                .short('p')
                .long("plan")
                .help("Purge plan: YEARLY,MONTHLY,WEEKLY,DAILY")
                .num_args(1),
        )
        .arg(
            Arg::new("verbose")
                .short('v')
                .long("verbose")
                .help("Print verbose output")
                .num_args(0)
                .action(ArgAction::SetTrue),
        )
        .get_matches();

    // parse arguments
    let path = PathBuf::from(args.get_one::<String>("DIR").unwrap_or(&".".to_string()));
    let dry_run = args.get_flag("dry-run");
    let plan = match args.get_one::<String>("plan") {
        Some(plan) => match plan.parse::<Plan>() {
            Ok(plan) => plan,
            Err(e) => {
                eprintln!("Failed to parse prune plan from '{}': {}", plan, e);
                std::process::exit(1);
            }
        },
        None => Plan::default(),
    };
    let verbose = args.get_flag("verbose");

    // do the prune
    prune(path.as_path(), plan, dry_run, verbose)
}

/// The main prune function. Find `Item`s, handle the deletes and reporting.
///
/// This function first searches the path and finds all valid `Item`s, and then constucts the
/// `Breakdown`.
///
/// In normal mode (without `dry_run`) the `Item`s that the breakdown categorized to "prune"
/// category are deleted. If `dry_run` is enabled, nothing is deleted but `Item`s that *would* be
/// deleted in normal mode are printed to STDOUT.
fn prune(path: &Path, plan: Plan, dry_run: bool, verbose: bool) {
    let items = read_items(path).unwrap();
    let breakdown = Breakdown::new(items, &plan);

    if verbose {
        print_items(Some("Yearly:"), &breakdown.yearly, true);
        print_items(Some("Montly:"), &breakdown.monthly, true);
        print_items(Some("Weekly:"), &breakdown.weekly, true);
        print_items(Some("Daily:"), &breakdown.daily, true);
        print_items(Some("Prune:"), &breakdown.prune, true);
    }

    if dry_run {
        // If dry_run, only print the items. If verbose was set, the items have already been
        // printed and nothing needs to be done here.
        if !verbose {
            print_items(None, &breakdown.prune, false);
        }
    } else {
        // Normal run.
        let prune_cnt = breakdown.prune.len();
        if verbose {
            println!("Deleting in total {} items.", prune_cnt);
        }

        for (i, item) in breakdown.prune.into_iter().enumerate() {
            print!("Deleting {} ", item.path().to_str().unwrap());
            use std::io::Write as _;
            std::io::stdout().flush().unwrap();

            match item.delete() {
                Ok(()) => {
                    print!("OK");
                }
                Err(e) => {
                    eprintln!("Failed to remove file: {}", e);
                }
            };

            if verbose {
                println!(", {} items left.", prune_cnt - i - 1);
            } else {
                println!();
            }
        }
    }
}

/// Format and print all items to STDOUT.
fn print_items(heading: Option<&str>, items: &[Item], print_newline: bool) {
    if heading.is_some() {
        println!("{}", heading.unwrap());
    }

    for item in items {
        println!("{}", item.path().to_str().unwrap());
    }

    if print_newline {
        println!();
    }
}
