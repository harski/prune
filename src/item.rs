//! `Item` is the data unit, a file or a directory, that `prune` deals with.
//!
//! Usually the list of items in a directory is fetched by `prune::item::read_items();`
//!
//! ```
//! use prune::{read_items, Item};
//! use std::path::PathBuf;
//! let items = read_items(&PathBuf::from(".")).unwrap();
//! for item in items {
//!     // ...
//! }
//! ```

use crate::error;
use crate::fs_utils::recursive_remove;
use chrono::{NaiveDate, NaiveDateTime};
use std::cmp::Ordering;
use std::convert::TryFrom;
use std::fs;
use std::path::{Path, PathBuf};
use std::str::FromStr;

const DATE_FORMAT: &str = "%Y-%m-%dT%H-%M-%S";

/// Filesystem `Item` that represents a prunable object.
///
/// ```no_run
/// use prune::Item;
/// use std::str::FromStr;
///
/// let item = Item::from_str("2020-10-06T15-40-00");
/// ```
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Item {
    datetime: NaiveDateTime,
    path: PathBuf,
}

impl Item {
    /// Get Item date as `chrono::Datelike`.
    ///
    /// ```no_run
    /// use chrono::Datelike;
    /// use prune::Item;
    /// use std::str::FromStr;
    ///
    /// let item = Item::from_str("2020-10-06T15-40-00").unwrap();
    ///
    /// assert_eq!(item.date().year(), 2020);
    /// assert_eq!(item.date().month(), 10);
    /// assert_eq!(item.date().day(), 6);
    /// ```
    pub fn date(&self) -> NaiveDate {
        self.datetime.date()
    }

    /// Delete the directory or file backed by the `Item`.
    ///
    /// ```
    /// use prune::{read_items, Item};
    /// use std::path::PathBuf;
    /// use std::str::FromStr;
    ///
    /// let items = read_items(&PathBuf::from(".")).unwrap();
    ///
    /// // Delete all items.
    /// for item in items {
    ///     let _ = item.delete();
    /// }
    /// ```
    pub fn delete(self) -> error::Result<()> {
        recursive_remove(&self.path)
    }

    /// Get the `Item` path.
    ///
    /// ```no_run
    /// use prune::Item;
    /// use std::str::FromStr;
    ///
    /// let item = Item::from_str("2020-10-07T08-50-20").unwrap();
    ///
    /// assert_eq!(item.path().to_str().unwrap(), "./2020-10-07T08-50-20");
    /// ```
    pub fn path(&self) -> &Path {
        &self.path
    }
}

impl TryFrom<PathBuf> for Item {
    type Error = error::Error;

    fn try_from(path: PathBuf) -> Result<Self, Self::Error> {
        let file_name = match path.file_name() {
            Some(f) => f,
            None => return Err(error::Error::InvalidArgument),
        };

        let item = match file_name.to_str() {
            Some(s) => {
                let datetime = NaiveDateTime::parse_from_str(s, DATE_FORMAT)?;
                Item { datetime, path }
            }
            None => {
                // in case the path parameter (PathBuf) was not from a utf-8 string and cannot be
                // converted to one.
                return Err(error::Error::InvalidArgument);
            }
        };

        Ok(item)
    }
}

impl FromStr for Item {
    type Err = chrono::format::ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let tmp = NaiveDateTime::parse_from_str(s, DATE_FORMAT)?;
        let path = PathBuf::from(s);
        Ok(Item {
            datetime: tmp,
            path,
        })
    }
}

impl Ord for Item {
    fn cmp(&self, other: &Self) -> Ordering {
        self.datetime.cmp(&other.datetime)
    }
}

impl PartialOrd for Item {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

/// Get the `Item`s in the directory `path`.
///
/// All files or directories in `path` that are named in the expected timestamped format are added
/// to the result vector. Other files are ignored.
///
/// ```
/// use prune::read_items;
/// use std::path::PathBuf;
///
/// let path_buf = PathBuf::from(".");
/// let items = read_items(&path_buf).unwrap();
/// ```
pub fn read_items(path: &Path) -> error::Result<Vec<Item>> {
    let mut items = Vec::new();

    // check that path exists
    for entry in fs::read_dir(&path)? {
        // bail out if reading a directory entry fails, bail out
        let entry = entry?;
        if let Ok(i) = Item::try_from(entry.path()) {
            items.push(i);
        }
    }

    items.sort();
    Ok(items)
}
