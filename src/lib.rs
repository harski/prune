//! Prune timestamped files or directories based on a set plan.
//!
//! Prune is a crate and a program for finding and deleting timestamped files or directories based
//! on a given retention plan. It is useful for example for keeping a certain set of backups on
//! disk and deleting the rest.
//!
//! The three main data structures for this crate are `Item`, `Plan` and `Breakdown`. The `Item`s
//! in the filesystem are sorted to different `Breakdown` categories according to the selected
//! `Plan`.
//!
//!
//! ## Example
//!
//! ```no_run
//! use prune::{read_items, Breakdown, Plan};
//! use std::path::PathBuf;
//!
//! let items = read_items(&PathBuf::from(".")).unwrap();
//! let plan = Plan::default();
//! let breakdown = Breakdown::new(items, &plan);
//!
//! for item in breakdown.prune {
//!     println!("File {} would be deleted.", item.path().to_str().unwrap());
//! }
//! ```

extern crate chrono;

mod breakdown;
mod error;
mod fs_utils;
mod item;

pub use breakdown::{Breakdown, Plan};
pub use error::{Error, Result};
pub use item::{read_items, Item};
