use crate::error::{FileContext, Result};
use std::fs;
use std::path::Path;

/// Remove the directory (file) in path recursively.
///
/// The function walks the directory tree and tries to delete everything recursively. In contrast
/// to `std::fs::remove_dir_all()` this function will try to delete empty, chmod 0000 directories
/// from the directory tree.
pub fn recursive_remove(path: &Path) -> Result<()> {
    let metadata = match path.symlink_metadata() {
        Ok(metadata) => metadata,
        Err(e) => return Err(FileContext::new_error(path.to_owned(), e)),
    };

    if !metadata.is_dir() {
        // handle regular files and symlinks
        if let Err(e) = fs::remove_file(path) {
            return Err(FileContext::new_error(path.to_owned(), e));
        }
    } else {
        // handle directories

        // Handle empty directory.
        match fs::remove_dir(path) {
            Ok(()) => return Ok(()),
            Err(e) => {
                // Ignore "directory not empty" errors. Recursive deletion is handled later. This
                // check exits to allow the deletion of empty, chmod 0000 permission directories.
                // Other errors should be reported.
                //
                // Note: fs::remove_dir_all() wouldn't handle this case at all.
                //
                // The `e.kind().to_string()` hack is needed because `Kind::DirectoryNotEmpty`
                // had leaked into stable. The kind can be emmitted, but not matched against.
                // TODO: follow https://github.com/rust-lang/rust/issues/86442 and remove hack when
                // `io_error_more` is merged.
                if !(e.kind().to_string().eq("directory not empty") && e.raw_os_error() == Some(39))
                {
                    return Err(FileContext::new_error(path.to_owned(), e));
                }
            }
        }

        // Handle non-empty directory.
        let dirents = match fs::read_dir(path) {
            Ok(entries) => entries,
            Err(e) => return Err(FileContext::new_error(path.to_owned(), e)),
        };

        for entry in dirents {
            let entry = match entry {
                Ok(entry) => entry,
                Err(e) => return Err(FileContext::new_error(path.to_owned(), e)),
            };
            recursive_remove(&entry.path())?;
        }

        // Finally, remove the emptied directory.
        if let Err(e) = fs::remove_dir(path) {
            return Err(FileContext::new_error(path.to_owned(), e));
        }
    }

    Ok(())
}
