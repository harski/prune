//! Error enum variants and the prune `Result` type.
//!
//! This crate uses the enum error pattern for wrapping all other errors to custom error type.

use std::error;
use std::fmt;
use std::path::PathBuf;

/// Ok or prune Error variant.
pub type Result<T> = std::result::Result<T, Error>;

/// Wrapper for file related io-errors, containing the file path in question.
#[derive(Debug)]
pub struct FileContext {
    pub path: PathBuf,
    pub source: std::io::Error,
}

impl FileContext {
    pub fn new_error(path: PathBuf, source: std::io::Error) -> Error {
        let context = FileContext { path, source };

        Error::FileIo(context)
    }
}

/// Error variants.
#[derive(Debug)]
pub enum Error {
    FileIo(FileContext),
    InvalidArgument,
    Io(std::io::Error),
    NotFound,
    Parse(chrono::ParseError),
    ParseInt(std::num::ParseIntError),
    ParsePlan,
}

impl fmt::Display for FileContext {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let path_str = self.path.to_str().unwrap_or("Unknown file");
        write!(f, "Error handling file '{}': {}", path_str, self.source)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::FileIo(ref e) => e.fmt(f),
            Error::InvalidArgument => write!(f, "Invalid argument."),
            Error::Io(ref e) => e.fmt(f),
            Error::NotFound => write!(f, "Resource not found."),
            Error::Parse(ref e) => e.fmt(f),
            Error::ParseInt(ref e) => e.fmt(f),
            Error::ParsePlan => write!(f, "Error parsing plan input."),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            Error::FileIo(ref e) => Some(&e.source),
            Error::InvalidArgument => None,
            Error::Io(ref e) => Some(e),
            Error::NotFound => None,
            Error::Parse(ref e) => Some(e),
            Error::ParseInt(ref e) => Some(e),
            Error::ParsePlan => None,
        }
    }
}

impl From<chrono::ParseError> for Error {
    fn from(err: chrono::ParseError) -> Error {
        Error::Parse(err)
    }
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Error {
        Error::Io(err)
    }
}

impl From<std::num::ParseIntError> for Error {
    fn from(err: std::num::ParseIntError) -> Error {
        Error::ParseInt(err)
    }
}
