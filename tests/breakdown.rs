use prune::{Breakdown, Item, Plan};
use std::str::FromStr;

#[test]
fn test_empty_plan_and_items() {
    let items = Vec::new();
    let plan = Plan::default();
    let bd = Breakdown::new(items, &plan);

    assert_eq!(0, bd.yearly.len());
    assert_eq!(0, bd.monthly.len());
    assert_eq!(0, bd.weekly.len());
    assert_eq!(0, bd.daily.len());
}

#[test]
fn test_daily_limit() {
    let mut items = Vec::new();
    items.push(Item::from_str("2019-07-04T14-23-29").unwrap());
    items.push(Item::from_str("2014-06-14T14-23-29").unwrap());
    items.push(Item::from_str("2019-06-04T14-23-29").unwrap());
    items.push(Item::from_str("2018-06-04T14-23-29").unwrap());
    items.push(Item::from_str("2019-04-04T14-23-29").unwrap());

    let plan = Plan {
        yearly: 0,
        monthly: 0,
        weekly: 0,
        daily: 3,
    };
    let bd = Breakdown::new(items, &plan);

    assert_eq!(0, bd.yearly.len());
    assert_eq!(0, bd.monthly.len());
    assert_eq!(0, bd.weekly.len());
    assert_eq!(3, bd.daily.len());

    assert_eq!(bd.daily[0], Item::from_str("2019-04-04T14-23-29").unwrap());
    assert_eq!(bd.daily[1], Item::from_str("2019-06-04T14-23-29").unwrap());
    assert_eq!(bd.daily[2], Item::from_str("2019-07-04T14-23-29").unwrap());
}

#[test]
fn test_monthly_limit() {
    let mut items = Vec::new();
    items.push(Item::from_str("2019-07-04T14-23-29").unwrap());
    items.push(Item::from_str("2019-04-01T14-23-29").unwrap());
    items.push(Item::from_str("2019-04-24T14-23-29").unwrap());
    items.push(Item::from_str("2019-03-16T14-23-29").unwrap());
    items.push(Item::from_str("2019-06-11T14-23-29").unwrap());
    items.push(Item::from_str("2019-04-29T14-23-29").unwrap());
    items.push(Item::from_str("2019-03-30T14-23-29").unwrap());
    items.push(Item::from_str("2019-02-08T14-23-29").unwrap());
    items.push(Item::from_str("2019-07-12T14-23-29").unwrap());

    let plan = Plan {
        yearly: 0,
        monthly: 3,
        weekly: 0,
        daily: 0,
    };
    let bd = Breakdown::new(items, &plan);

    assert_eq!(0, bd.yearly.len());
    assert_eq!(3, bd.monthly.len());
    assert_eq!(0, bd.weekly.len());
    assert_eq!(0, bd.daily.len());

    assert_eq!(
        bd.monthly[0],
        Item::from_str("2019-04-29T14-23-29").unwrap()
    );
    assert_eq!(
        bd.monthly[1],
        Item::from_str("2019-06-11T14-23-29").unwrap()
    );
    assert_eq!(
        bd.monthly[2],
        Item::from_str("2019-07-12T14-23-29").unwrap()
    );
}

#[test]
fn test_no_duplicates() {
    let mut items = Vec::new();
    items.push(Item::from_str("2019-07-04T14-23-29").unwrap());
    items.push(Item::from_str("2014-06-14T14-23-29").unwrap());
    items.push(Item::from_str("2019-06-04T14-23-29").unwrap());
    items.push(Item::from_str("2018-06-04T14-23-29").unwrap());
    items.push(Item::from_str("2019-04-04T14-23-29").unwrap());

    let plan = Plan {
        yearly: 1,
        monthly: 1,
        weekly: 1,
        daily: 1,
    };
    let bd = Breakdown::new(items, &plan);

    assert_eq!(1, bd.prune.len());
    assert_eq!(1, bd.yearly.len());
    assert_eq!(1, bd.monthly.len());
    assert_eq!(1, bd.weekly.len());
    assert_eq!(1, bd.daily.len());
}

#[test]
fn test_plan_from_str() {
    assert!(Plan::from_str("10,6,4,7").is_ok());
    assert!(Plan::from_str("10, 6, 4, 7").is_ok());
    assert!(Plan::from_str(" 10 , 6, 4, 7 ").is_ok());
    assert!(Plan::from_str("10,6,4,7,1").is_err());
    assert!(Plan::from_str("10,6,4").is_err());
    assert!(Plan::from_str("10,6,4,").is_err());
}

#[test]
fn test_same_day() {
    let mut items = Vec::new();
    items.push(Item::from_str("2019-06-14T00-00-00").unwrap());
    items.push(Item::from_str("2019-06-14T23-59-59").unwrap());
    items.push(Item::from_str("2019-06-14T12-00-00").unwrap());

    let plan = Plan {
        yearly: 0,
        monthly: 0,
        weekly: 0,
        daily: 3,
    };
    let bd = Breakdown::new(items, &plan);

    assert_eq!(0, bd.yearly.len());
    assert_eq!(0, bd.monthly.len());
    assert_eq!(0, bd.weekly.len());
    assert_eq!(1, bd.daily.len());

    assert_eq!(Item::from_str("2019-06-14T23-59-59").unwrap(), bd.daily[0]);
}

#[test]
fn test_weekly_limit() {
    let mut items = Vec::new();
    items.push(Item::from_str("2019-06-28T00-00-00").unwrap());
    items.push(Item::from_str("2019-07-01T14-23-29").unwrap());
    items.push(Item::from_str("2019-06-16T14-23-29").unwrap());
    items.push(Item::from_str("2019-06-02T14-23-29").unwrap());
    items.push(Item::from_str("2019-06-26T00-00-00").unwrap());
    items.push(Item::from_str("2019-06-18T14-23-29").unwrap());
    items.push(Item::from_str("2019-06-08T00-00-00").unwrap());
    items.push(Item::from_str("2019-07-07T14-23-29").unwrap());

    let plan = Plan {
        yearly: 0,
        monthly: 0,
        weekly: 3,
        daily: 0,
    };
    let bd = Breakdown::new(items, &plan);

    assert_eq!(0, bd.yearly.len());
    assert_eq!(0, bd.monthly.len());
    assert_eq!(3, bd.weekly.len());
    assert_eq!(0, bd.daily.len());

    assert_eq!(bd.weekly[0], Item::from_str("2019-06-18T14-23-29").unwrap());
    assert_eq!(bd.weekly[1], Item::from_str("2019-06-28T00-00-00").unwrap());
    assert_eq!(bd.weekly[2], Item::from_str("2019-07-07T14-23-29").unwrap());
}

#[test]
fn test_with_plan_and_empty_items() {
    let items = Vec::new();
    let plan = Plan {
        yearly: 10,
        monthly: 6,
        weekly: 4,
        daily: 5,
    };
    let bd = Breakdown::new(items, &plan);

    assert_eq!(0, bd.yearly.len());
    assert_eq!(0, bd.monthly.len());
    assert_eq!(0, bd.weekly.len());
    assert_eq!(0, bd.daily.len());
}

#[test]
fn test_yearly_limit() {
    let mut items = Vec::new();
    items.push(Item::from_str("2019-06-28T00-00-00").unwrap());
    items.push(Item::from_str("2009-07-01T14-23-29").unwrap());
    items.push(Item::from_str("2011-06-16T14-23-29").unwrap());
    items.push(Item::from_str("2014-06-02T14-23-29").unwrap());
    items.push(Item::from_str("2019-06-26T00-00-00").unwrap());
    items.push(Item::from_str("2004-06-18T14-23-29").unwrap());
    items.push(Item::from_str("2013-06-08T00-00-00").unwrap());
    items.push(Item::from_str("2017-05-05T14-23-29").unwrap());
    items.push(Item::from_str("2017-07-07T14-23-29").unwrap());

    let plan = Plan {
        yearly: 3,
        monthly: 0,
        weekly: 0,
        daily: 0,
    };
    let bd = Breakdown::new(items, &plan);

    assert_eq!(3, bd.yearly.len());
    assert_eq!(0, bd.monthly.len());
    assert_eq!(0, bd.weekly.len());
    assert_eq!(0, bd.daily.len());

    assert_eq!(bd.yearly[0], Item::from_str("2014-06-02T14-23-29").unwrap());
    assert_eq!(bd.yearly[1], Item::from_str("2017-07-07T14-23-29").unwrap());
    assert_eq!(bd.yearly[2], Item::from_str("2019-06-28T00-00-00").unwrap());
}
