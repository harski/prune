use prune::Item;
use std::convert::TryFrom;
use std::fs::{create_dir, read_dir, File};
use std::path::{Path, PathBuf};
use tempfile::tempdir;

fn create_dirs(path: &Path, v: &[&str]) {
    for d in v {
        let mut new_dir = PathBuf::from(&path);
        new_dir.push(d);
        create_dir(&new_dir).unwrap();
    }
}

fn create_files(path: &Path, v: &[&str]) {
    for f in v {
        let mut new_file = PathBuf::from(&path);
        new_file.push(f);
        File::create(new_file).unwrap();
    }
}

fn create_filetree(root: &Path, delete_dir: &str) {
    let mut base_path = PathBuf::from(&root);
    base_path.push(delete_dir);
    create_dir(&base_path).unwrap();

    let files = vec!["1", "2", "3"];
    let dirs = vec!["a", "b", "c"];

    create_files(&base_path, &files);
    create_dirs(&base_path, &dirs);

    for tmp_dir in dirs {
        let mut subdir = PathBuf::from(&base_path);
        subdir.push(tmp_dir);
        create_files(&subdir, &files);
    }
}

fn is_dir_empty(path: &Path) -> bool {
    read_dir(path).unwrap().count() == 0
}

/// Test the Item::FromStr (parse) trait.
#[test]
fn test_datetime_parse() {
    assert!("2019-07-04T14-23-29".parse::<Item>().is_ok());
    assert!("2019-07-04T00-00-00".parse::<Item>().is_ok());
    assert!("2019-07-04T00-00-000".parse::<Item>().is_err());
    assert!("2019-02-29T00-00-00".parse::<Item>().is_err());
}

/// Test that Item deletion works for a file tree containing dirs and files.
#[test]
fn test_file_tree_deletion() {
    let tmp_dir = tempdir().unwrap();
    let delete_dir_str = "2020-10-06T00-00-00";

    assert!(is_dir_empty(&tmp_dir.path()));
    create_filetree(&tmp_dir.path(), delete_dir_str);
    assert!(!is_dir_empty(&tmp_dir.path()));

    let mut delete_dir = PathBuf::from(&tmp_dir.path());
    delete_dir.push(delete_dir_str);
    let item = Item::try_from(delete_dir).unwrap();
    item.delete().unwrap();
    assert!(is_dir_empty(&tmp_dir.path()));
}

/// Test deletion of empty Item directory.
#[test]
fn test_empty_dir_deletion() {
    let tmp_dir = tempdir().unwrap();
    let delete_dir_str = "2020-10-06T00-00-00";
    let mut path = PathBuf::from(tmp_dir.path());
    path.push(delete_dir_str);

    create_dir(&path).unwrap();
    let item = Item::try_from(path).unwrap();

    assert!(item.delete().is_ok());
}

#[test]
fn test_invalid_path() {
    let path = PathBuf::from("test/..");
    let item = Item::try_from(path.clone());
    assert!(item.is_err());
}

#[test]
fn test_path_parser() {
    let path = PathBuf::from("2020-10-06T00-00-00");
    let item = Item::try_from(path.clone()).unwrap();
    assert_eq!(&path, item.path());
}

#[test]
fn test_read_items() {
    let item_dir_str = "2020-10-06T00-00-00";
    let tmp_dir = tempdir().unwrap();
    let mut path = PathBuf::from(tmp_dir.path());
    path.push(item_dir_str);

    create_dir(&path).unwrap();
    let items = prune::read_items(tmp_dir.path());

    assert_eq!(1, items.unwrap().len());
}

#[test]
fn test_read_items_empty() {
    let tmp_dir = tempdir().unwrap();
    let path = PathBuf::from(tmp_dir.path());
    let items = prune::read_items(&path);
    assert!(items.is_ok());
}
