use assert_cmd;
use std::fs::{read_dir, File};
use std::path::{Path, PathBuf};
use tempfile;

const FILE_CNT: usize = 31;

fn touch_month(root: &Path) {
    let prefix = "2020-04-";
    let suffix = "T00-00-00";

    for i in 1..=FILE_CNT {
        let mut stamp = prefix.to_string();

        if i < 10 {
            stamp.push_str("0");
        }

        stamp.push_str(&i.to_string());
        stamp.push_str(suffix);

        let mut file_path = PathBuf::from(root);
        file_path.push(stamp);
        File::create(file_path).unwrap();
    }
}

/// Test that dry run won't delete any files.
#[test]
pub fn dry_run() {
    let root = tempfile::tempdir().unwrap();
    touch_month(root.path());
    let file_cnt_before = read_dir(root.path()).unwrap().count();
    assert_eq!(FILE_CNT, file_cnt_before);

    let mut cmd = assert_cmd::Command::cargo_bin(assert_cmd::crate_name!()).unwrap();
    cmd.arg("--dry-run")
        .arg(root.path().to_str().unwrap())
        .assert()
        .success();

    let file_cnt_after = read_dir(root.path()).unwrap().count();

    assert_eq!(file_cnt_before, file_cnt_after);
}

/// Test that normal prune run succeeds.
#[test]
pub fn plan_run() {
    let root = tempfile::tempdir().unwrap();
    touch_month(root.path());
    let file_cnt_before = read_dir(root.path()).unwrap().count();
    assert_eq!(FILE_CNT, file_cnt_before);

    let mut cmd = assert_cmd::Command::cargo_bin(assert_cmd::crate_name!()).unwrap();
    cmd.arg("--plan")
        .arg("1,2,2,5")
        .arg(root.path().to_str().unwrap())
        .assert()
        .success();

    let file_cnt_after = read_dir(root.path()).unwrap().count();

    assert_eq!(10, file_cnt_after);
}
