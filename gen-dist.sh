#!/bin/bash

target=prune
dir=prune

# create dist dir
mkdir -p $dir

# copy relevant files
arr=( \
	Cargo.lock \
	Cargo.toml \
	CHANGELOG.md \
	LICENSE \
	README.md \
	src \
	tests \
)

for file in "${arr[@]}"; do
	cp -r $file $dir
done

# create the dist file
tar -czf $target.tar.gz $dir

# remove dist dir
rm -r $dir
